package com.example.dilan.typicode.ui.photos_list

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.example.dilan.typicode.Constants
import com.example.dilan.typicode.R
import com.example.dilan.typicode.network.PhotosResponse
import com.example.dilan.typicode.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_photos.*
import kotlinx.android.synthetic.main.toolbar.*

class PhotosActivity : BaseActivity<PhotosContract.View, PhotosPresenter>(), PhotosContract.View {

    private val resources: ArrayList<PhotosResponse> = ArrayList()
    override var mPresenter: PhotosPresenter = PhotosPresenter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photos)
        mPresenter.loadPhotos(intent.getIntExtra(Constants.ALBUMS_ID, 0))
        setTitleToolbar(getString(R.string.images))
        backBtn.setOnClickListener { onBackPressed() }
    }


    override fun showPhotos(photos: List<PhotosResponse>) {
        resources.addAll(photos)
        setUpRecyclerView()

    }

    private fun setUpRecyclerView() {
        photosList.layoutManager = GridLayoutManager(this, 3)
        photosList.adapter = PhotoAdapter(this, resources)
    }

    private fun setTitleToolbar(title: String) {
        toolbarTitle.text = title
        setSupportActionBar(toolbar)
    }
}