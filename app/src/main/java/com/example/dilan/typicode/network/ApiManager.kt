package com.example.dilan.typicode.network


import com.example.dilan.typicode.BuildConfig
import com.example.dilan.typicode.Constants
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ApiManager {
    private var serviceApi: ServiceApi? = null

    private var weatherServiceApi: WeatherApi? = null

    init {
        val typicodeRetrofit = typicodeRetrofit()
        initTypicode(typicodeRetrofit)
        val weatherRetrofit = weatherRetrofit()
        initWeather(weatherRetrofit)
    }

    private fun okHttp(): OkHttpClient.Builder {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(loggingInterceptor)
        }

        return builder.apply {

            networkInterceptors().add(Interceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                        .method(original.method(), original.body())
                        .build()

                chain.proceed(request)
            })
        }
    }

    private fun typicodeRetrofit(): Retrofit =
            Retrofit.Builder()
                    .addConverterFactory(createMoshiConverter())
                    .baseUrl(Constants.BASE_URL)
                    .client(okHttp().build())
                    .build()

    private fun weatherRetrofit(): Retrofit =
            Retrofit.Builder()
                    .addConverterFactory(createMoshiConverter())
                    .baseUrl(Constants.W_URL)
                    .client(okHttp().build())
                    .build()


    private fun createMoshiConverter(): MoshiConverterFactory = MoshiConverterFactory.create()


    private fun initTypicode(retrofit: Retrofit) {
        serviceApi = retrofit.create(ServiceApi::class.java)
    }

    private fun initWeather(retrofit: Retrofit) {
        weatherServiceApi = retrofit.create(WeatherApi::class.java)
    }

    fun getPosts() = serviceApi?.getListPosts()

    fun getComments(id: Int) = serviceApi?.getComments(id)

    fun getAlbums() = serviceApi?.getAlbums()

    fun getPhotos(id: Int) = serviceApi?.getPhotos(id)

    fun getWeather(parameters: Map<String, String>) = weatherServiceApi?.getListWeather(parameters)
}