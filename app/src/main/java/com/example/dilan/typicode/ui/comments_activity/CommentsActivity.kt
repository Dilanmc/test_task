package com.example.dilan.typicode.ui.comments_activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.dilan.typicode.Constants
import com.example.dilan.typicode.R
import com.example.dilan.typicode.network.CommentsResponse
import com.example.dilan.typicode.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_comments.*
import kotlinx.android.synthetic.main.toolbar.*

class CommentsActivity : BaseActivity<CommentsContract.View,
        CommentsContract.Presenter>(),
        CommentsContract.View {

    override var mPresenter: CommentsContract.Presenter = CommentsPresenter()
    private val resources: ArrayList<CommentsResponse> = ArrayList()


    override fun showComments(comments: List<CommentsResponse>) {
        resources.addAll(comments)
        setUpRecyclerView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)
        mPresenter.loadComments(intent.getIntExtra(Constants.POST_ID, 0))
        setTitleToolbar(getString(R.string.comments))
        backBtn.setOnClickListener { onBackPressed() }
    }

    private fun setUpRecyclerView() {
        commentList.layoutManager = LinearLayoutManager(this)
        commentList.adapter = CommentAdapter(this, resources)
    }

    private fun setTitleToolbar(title: String) {
        toolbarTitle.text = title
        setSupportActionBar(toolbar)
    }
}