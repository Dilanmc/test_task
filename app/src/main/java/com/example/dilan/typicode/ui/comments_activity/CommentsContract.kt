package com.example.dilan.typicode.ui.comments_activity

import com.example.dilan.typicode.network.CommentsResponse
import com.example.dilan.typicode.ui.base.BaseView
import com.example.dilan.typicode.ui.base.BasePresenter

object CommentsContract {
    interface View : BaseView {
        fun showComments(comments: List<CommentsResponse>)
    }

    interface Presenter : BasePresenter<View> {
        fun loadComments(id: Int?)
    }

}