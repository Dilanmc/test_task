package com.example.dilan.typicode.ui.photos_list

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.dilan.typicode.Constants
import com.example.dilan.typicode.R
import com.example.dilan.typicode.network.PhotosResponse
import kotlinx.android.synthetic.main.item_photo.view.*

class PhotoAdapter(private val context: Context,
                   private val data: List<PhotosResponse>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false)
        return Item(v)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Item).bindData(data[position], context)

    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(photo: PhotosResponse, context: Context) {
            Glide.with(context)
                    .load(photo.thumbnailUrl)
                    .into(itemView.photo)
            itemView.setOnClickListener {
                context.startActivity(Intent(context, PhotoDetailActivity::class.java)
                        .putExtra(Constants.IMAGE, photo))
            }
        }
    }
}