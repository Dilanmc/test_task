package com.example.dilan.typicode.ui

import android.os.Bundle
import com.example.dilan.typicode.R
import com.example.dilan.typicode.ui.albums_fragment.AlbumsFragment
import com.example.dilan.typicode.ui.base.AbstractMainActivity
import com.example.dilan.typicode.ui.posts_fragment.PostsListFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AbstractMainActivity() {
    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addFragment(R.id.frame, PostsListFragment.newInstance(), "")
        setTitleToolbar(getString(R.string.posts1))

        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_post -> {
                    replaceFragment(R.id.frame, PostsListFragment.newInstance(), "")
                    setTitleToolbar(getString(R.string.posts1))
                }
                R.id.action_albums -> {
                    replaceFragment(R.id.frame, AlbumsFragment.newInstance(), "")
                    setTitleToolbar(getString(R.string.albums1))
                }
            }
            true
        }
    }

    private fun setTitleToolbar(title: String) {
        toolbar.title = title
        setSupportActionBar(toolbar)
    }
}
