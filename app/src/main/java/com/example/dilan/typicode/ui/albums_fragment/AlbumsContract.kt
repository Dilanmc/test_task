package com.example.dilan.typicode.ui.albums_fragment

import com.example.dilan.typicode.network.AlbumsResponse
import com.example.dilan.typicode.ui.base.BaseView
import com.example.dilan.typicode.ui.base.BasePresenter

object AlbumsContract {

    interface View : BaseView {
        fun showAlbums(albums: List<AlbumsResponse>)
    }

    interface Presenter : BasePresenter<View> {
        fun loadAlbums()
    }
}