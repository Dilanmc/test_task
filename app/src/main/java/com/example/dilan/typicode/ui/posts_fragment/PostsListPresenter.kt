package com.example.dilan.typicode.ui.posts_fragment

import com.example.dilan.typicode.Constants.api_key
import com.example.dilan.typicode.network.ApiManager
import com.example.dilan.typicode.network.PostResponse
import com.example.dilan.typicode.network.WeatherModel
import com.example.dilan.typicode.ui.base.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class PostsListPresenter : BasePresenterImpl<PostsListContract.View>(), PostsListContract.Presenter {
    override fun loadWeather() {
        ApiManager.getWeather(getFilters())?.enqueue(object : Callback<WeatherModel>{
            override fun onFailure(call: Call<WeatherModel>?, t: Throwable?) {
                mView?.showError(t.toString())
            }

            override fun onResponse(call: Call<WeatherModel>?, response: Response<WeatherModel>?) {
                mView?.showWeather(response?.body()!!)
            }

        })

    }

    override fun loadPosts() {
        ApiManager.getPosts()?.enqueue(object : Callback<List<PostResponse>> {
            override fun onFailure(call: Call<List<PostResponse>>?, t: Throwable?) {
                mView?.showError(t.toString())
            }

            override fun onResponse(call: Call<List<PostResponse>>?, response: Response<List<PostResponse>>?) {
                mView?.showPost(response?.body()!!)

            }

        })
    }

    private fun getFilters(): Map<String, String> {
        val parameters = HashMap<String, String>()
        parameters["APPID"] = api_key
        parameters["q"] = "Bishkek"
        parameters["units"] = "metric"
        return parameters
    }
}