package com.example.dilan.typicode.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface WeatherApi {
    @GET("weather")
    fun getListWeather(@QueryMap parameters: Map<String, String>): Call<WeatherModel>
}