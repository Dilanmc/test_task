package com.example.dilan.typicode.ui.albums_fragment

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.dilan.typicode.Constants
import com.example.dilan.typicode.R
import com.example.dilan.typicode.network.AlbumsResponse
import com.example.dilan.typicode.ui.photos_list.PhotosActivity
import kotlinx.android.synthetic.main.item_albums.view.*

class AlbumsAdapter(private val context: Context,
                    private val data: List<AlbumsResponse>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_albums, parent, false)
        return Item(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AlbumsAdapter.Item).bindData(data[position], context)
    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(album: AlbumsResponse, context: Context) {
            itemView.albumTitle.text = album.title
            itemView.setOnClickListener {
                context.startActivity(Intent(context, PhotosActivity::class.java).putExtra(Constants.ALBUMS_ID, album.id))
            }
        }
    }
}