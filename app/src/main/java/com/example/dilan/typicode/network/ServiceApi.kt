package com.example.dilan.typicode.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ServiceApi {
    @GET("posts")
    fun getListPosts(): Call<List<PostResponse>>

    @GET("posts/{id}/comments")
    fun getComments(@Path("id") id: Int): Call<List<CommentsResponse>>

    @GET("albums")
    fun getAlbums(): Call<List<AlbumsResponse>>

    @GET("albums/{id}/photos")
    fun getPhotos(@Path("id") id: Int): Call<List<PhotosResponse>>

}