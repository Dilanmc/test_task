package com.example.dilan.typicode.ui.comments_activity

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.dilan.typicode.R
import com.example.dilan.typicode.network.CommentsResponse
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentAdapter(private val context: Context,
                     private val data: List<CommentsResponse>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false)
        return Item(v)
    }

    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Item).bindData(data[position], context)
    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(comments: CommentsResponse, context: Context) {
            itemView.commentTitle.text = comments.body
            itemView.name.text = comments.name
            itemView.email.text = comments.email
        }
    }
}