package com.example.dilan.typicode.ui.posts_fragment

import com.example.dilan.typicode.network.PostResponse
import com.example.dilan.typicode.network.WeatherList
import com.example.dilan.typicode.network.WeatherModel
import com.example.dilan.typicode.ui.base.BasePresenter
import com.example.dilan.typicode.ui.base.BaseView

object PostsListContract {
    interface View : BaseView {
        fun showPost(posts: List<PostResponse>)
        fun showWeather(weather: WeatherModel)
    }

    interface Presenter : BasePresenter<View> {
        fun loadPosts()
        fun loadWeather()
    }
}