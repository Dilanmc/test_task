package com.example.dilan.typicode.network

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

data class PostResponse(
        val userId: Int?,
        val id: Int?,
        val title: String?,
        val body: String?
)

data class CommentsResponse(
        val postId: Int?,
        val id: Int?,
        val name: String?,
        val email: String?,
        val body: String?
)

data class AlbumsResponse(
        val userId: Int?,
        val id: Int?,
        val title: String?
)

@Parcelize
data class PhotosResponse(
        val albumId: Int?,
        val id: Int?,
        val title: String?,
        val url: String?,
        val thumbnailUrl: String?
) : Parcelable


data class WeatherModel(
        val id: Int,
        val name: String,
        @Json(name = "dt_txt")
        val dtTxt: String,
        val main: Data,
        val weather: List<WeatherDetail>
)

data class Data(
        val temp: Double,
        @Json(name = "temp_max")
        val tempMax: Double,
        @Json(name = "temp_min")
        val tempMin: Double,
        val pressure: Double
)

data class WeatherDetail(
        val description: String
)

data class WeatherList(
        val list: ArrayList<WeatherModel>
)


