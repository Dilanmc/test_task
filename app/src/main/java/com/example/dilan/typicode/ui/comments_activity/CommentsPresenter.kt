package com.example.dilan.typicode.ui.comments_activity

import com.example.dilan.typicode.network.ApiManager
import com.example.dilan.typicode.network.CommentsResponse
import com.example.dilan.typicode.ui.base.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommentsPresenter : BasePresenterImpl<CommentsContract.View>(), CommentsContract.Presenter {
    override fun loadComments(id: Int?) {
        ApiManager.getComments(id!!)?.enqueue(object : Callback<List<CommentsResponse>> {
            override fun onFailure(call: Call<List<CommentsResponse>>?, t: Throwable?) {
                mView?.showError(t.toString())
            }

            override fun onResponse(call: Call<List<CommentsResponse>>?, response: Response<List<CommentsResponse>>?) {
                mView?.showComments(response?.body()!!)
            }
        })
    }
}