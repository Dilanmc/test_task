package com.example.dilan.typicode.ui.albums_fragment

import com.example.dilan.typicode.network.AlbumsResponse
import com.example.dilan.typicode.network.ApiManager
import com.example.dilan.typicode.ui.base.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AlbumsPresenter : BasePresenterImpl<AlbumsContract.View>(), AlbumsContract.Presenter {
    override fun loadAlbums() {
        ApiManager.getAlbums()?.enqueue(object : Callback<List<AlbumsResponse>> {
            override fun onFailure(call: Call<List<AlbumsResponse>>?, t: Throwable?) {
                mView?.showError(t.toString())
            }

            override fun onResponse(call: Call<List<AlbumsResponse>>?, response: Response<List<AlbumsResponse>>?) {
                mView?.showAlbums(response?.body()!!)
            }

        })
    }

}