package com.example.dilan.typicode.ui.photos_list

import com.example.dilan.typicode.network.ApiManager
import com.example.dilan.typicode.network.PhotosResponse
import com.example.dilan.typicode.ui.base.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PhotosPresenter : BasePresenterImpl<PhotosContract.View>(), PhotosContract.Presenter {
    override fun loadPhotos(id: Int?) {
        ApiManager.getPhotos(id!!)?.enqueue(object : Callback<List<PhotosResponse>> {
            override fun onFailure(call: Call<List<PhotosResponse>>?, t: Throwable?) {
                mView?.showError(t.toString())
            }

            override fun onResponse(call: Call<List<PhotosResponse>>?, response: Response<List<PhotosResponse>>?) {
                mView?.showPhotos(response?.body()!!)
            }

        })
    }
}