package com.example.dilan.typicode.ui.posts_fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.dilan.typicode.network.PostResponse
import com.example.dilan.typicode.network.WeatherModel
import com.example.dilan.typicode.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_list.*

class PostsListFragment : BaseFragment<PostsListContract.View, PostsListPresenter>(), PostsListContract.View {
    companion object {
        fun newInstance(): PostsListFragment {
            return PostsListFragment()
        }
    }

    private val postsResources: ArrayList<PostResponse> = ArrayList()
    private var weatherResources: WeatherModel? = null


    override var mPresenter: PostsListPresenter = PostsListPresenter()

    override fun showPost(posts: List<PostResponse>) {
        postsResources.addAll(posts)
    }

    override fun showWeather(weather: WeatherModel) {
        weatherResources = weather
        setupRecyclerView()

    }


    override fun onResume() {
        super.onResume()
        mPresenter.loadPosts()
        mPresenter.loadWeather()
    }

    private fun setupRecyclerView() {
        list.layoutManager = LinearLayoutManager(context)
        list.adapter = PostAdapter(context, postsResources, weatherResources!!)
    }

}