package com.example.dilan.typicode.ui.photos_list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.dilan.typicode.Constants
import com.example.dilan.typicode.R
import com.example.dilan.typicode.network.PhotosResponse
import kotlinx.android.synthetic.main.activity_photo_detail.*
import kotlinx.android.synthetic.main.toolbar.*

class PhotoDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_detail)
        val photo = intent.getParcelableExtra<PhotosResponse>(Constants.IMAGE)
        setTitleToolbar(getString(R.string.image))
        bindView(photo)
    }

    private fun bindView(photo: PhotosResponse) {
        Glide.with(this)
                .load(photo.thumbnailUrl)
                .into(photoDetail)
        photoTitle.text = photo.title
        backBtn.setOnClickListener { onBackPressed() }
    }

    private fun setTitleToolbar(title: String) {
        toolbarTitle.text = title
        setSupportActionBar(toolbar)
    }
}