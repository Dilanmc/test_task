package com.example.dilan.typicode.ui.albums_fragment

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.example.dilan.typicode.network.AlbumsResponse
import com.example.dilan.typicode.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_list.*

class AlbumsFragment : BaseFragment<AlbumsContract.View, AlbumsPresenter>(), AlbumsContract.View {
    companion object {
        fun newInstance(): AlbumsFragment {
            return AlbumsFragment()
        }
    }

    private val resources: ArrayList<AlbumsResponse> = ArrayList()
    override var mPresenter: AlbumsPresenter = AlbumsPresenter()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.loadAlbums()
    }


    override fun showAlbums(albums: List<AlbumsResponse>) {
        resources.addAll(albums)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        list.layoutManager = GridLayoutManager(context, 3)
        list.adapter = AlbumsAdapter(context, resources)
    }
}