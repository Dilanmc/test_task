package com.example.dilan.typicode.ui.posts_fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.dilan.typicode.Constants
import com.example.dilan.typicode.R
import com.example.dilan.typicode.network.PostResponse
import com.example.dilan.typicode.network.WeatherModel
import com.example.dilan.typicode.ui.comments_activity.CommentsActivity
import kotlinx.android.synthetic.main.item_post.view.*
import kotlinx.android.synthetic.main.item_weather.view.*

class PostAdapter(private val context: Context,
                  private val data: List<PostResponse>,
                  private val weather: WeatherModel)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val post = LayoutInflater.from(context).inflate(R.layout.item_post, parent, false)
        val weather = LayoutInflater.from(context).inflate(R.layout.item_weather, parent, false)

        when (viewType) {
            1 -> return Item(post)
            0 -> return Weather(weather)
        }
        return Item(post)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            1 -> (holder as Item).bindData(data[position], context)
            0 -> (holder as Weather).bindData(weather, context)
        }

    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(post: PostResponse, context: Context) {
            itemView.titlePost.text = post.title
            itemView.descriptionText.text = post.body
            itemView.setOnClickListener {
                context.startActivity(Intent(context, CommentsActivity::class.java).putExtra(Constants.POST_ID, post.id))
            }
        }
    }

    class Weather(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(weather: WeatherModel, context: Context) {
            itemView.name.text = weather.name
            itemView.date.text = weather.dtTxt
            itemView.tempMax.text = String.format("%.2f", weather.main.tempMax) + " ℃"
            itemView.tempMin.text = String.format("%.2f", weather.main.tempMin) + " ℃"
            itemView.weather.text = weather.weather[0].description
            itemView.setOnClickListener {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(Constants.WEB_URL)
                context.startActivity(i)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        var viewType = 1
        if (position == 0) viewType = 0
        return viewType
    }
}