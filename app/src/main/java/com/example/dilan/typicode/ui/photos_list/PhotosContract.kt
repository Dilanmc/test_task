package com.example.dilan.typicode.ui.photos_list

import com.example.dilan.typicode.network.PhotosResponse
import com.example.dilan.typicode.ui.base.BaseView
import com.example.dilan.typicode.ui.base.BasePresenter

object PhotosContract {
    interface View : BaseView {
        fun showPhotos(photos: List<PhotosResponse>)
    }

    interface Presenter : BasePresenter<View> {
        fun loadPhotos(id: Int?)
    }
}