package com.example.dilan.typicode

object Constants {
    const val BASE_URL = "http://jsonplaceholder.typicode.com/"
    const val ALBUMS_ID = "albumId"
    const val POST_ID = "postId"
    const val IMAGE = "imageId"
    const val W_URL = "http://api.openweathermap.org/data/2.5/"
    const val api_key: String = "973a6c0046d2fc5a445b2dfa82b4488e"
    const val WEB_URL: String = "https://o.kg/"
}